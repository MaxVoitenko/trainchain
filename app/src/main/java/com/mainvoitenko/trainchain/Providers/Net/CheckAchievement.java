package com.mainvoitenko.trainchain.Providers.Net;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.BitmapFactory;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CheckAchievement {

    private DatabaseReference myRef;
//    private FirebaseUser user;
    private Context context;
    private String[] achievement_name;
    private String[] achievement_description;
    private TypedArray achievement_image;
    private String userId;

    private DataSnapshot dataSnapshot;
    public static List<String> messages = new ArrayList<>();


    public CheckAchievement(DataSnapshot dataSnapshot, String userId, Context context) {
        this.dataSnapshot = dataSnapshot;
        this.myRef = FirebaseDatabase.getInstance().getReference();
        this.userId = userId;
        this.context = context;
        achievement_name = context.getResources().getStringArray(R.array.achievement_name); //имена
        achievement_description = context.getResources().getStringArray(R.array.achievement_description); //описания
        achievement_image = context.getResources().obtainTypedArray(R.array.achievement_drawble);//картинки
    }

public void firstTen(){
            if(dataSnapshot.getChildrenCount()<11){
                if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_TEN)).exists()){
                    myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_TEN)).setValue("1");
                notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_FIRST_TEN],
                        achievement_description[SettingsConstants.ACHIEVEMENT_FIRST_TEN],
                        achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_FIRST_TEN, 1));
                }
}
firstFifty();
firstOneHundred();
}
public void firstFifty() {
    if (dataSnapshot.getChildrenCount() < 51) {
        if (!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_FIFTY)).exists()) {
            myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_FIFTY)).setValue("1");
            notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_FIRST_FIFTY],
                    achievement_description[SettingsConstants.ACHIEVEMENT_FIRST_FIFTY],
//                        achievement_image.getDrawable(SettingsConstants.ACHIEVEMENT_FIRST_FIFTY));}
                    achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_FIRST_FIFTY, 1));
        }
    }
}
public void firstOneHundred() {
    if (dataSnapshot.getChildrenCount() < 101) {
        if (!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_ONE_HUNDRED)).exists()) {
            myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_ONE_HUNDRED)).setValue("1");
            notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_FIRST_ONE_HUNDRED],
                    achievement_description[SettingsConstants.ACHIEVEMENT_FIRST_ONE_HUNDRED],
                    achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_FIRST_ONE_HUNDRED, 1));
        }
    }
}

public void firstFriend(){
    if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_FRIEND)).exists()){
        if(dataSnapshot.child(userId).child(SettingsConstants.FRIENDS).exists()){
                    if(dataSnapshot.child(userId).child(SettingsConstants.FRIENDS).getChildrenCount() >= 1){
                        myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_FRIEND)).setValue("1");
                        notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_FIRST_FRIEND],
                                achievement_description[SettingsConstants.ACHIEVEMENT_FIRST_FRIEND],
                                achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_FIRST_FRIEND, 1));
                    }
                }
            }
    fiveFriends();
    tenFriends();
}
public void fiveFriends(){
    if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIVE_FRIENDS)).exists()){
        if(dataSnapshot.child(userId).child(SettingsConstants.FRIENDS).exists()){
                    if(dataSnapshot.child(userId).child(SettingsConstants.FRIENDS).getChildrenCount() >= 5){
                myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIVE_FRIENDS)).setValue("1");
                        notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_FIVE_FRIENDS],
                                achievement_description[SettingsConstants.ACHIEVEMENT_FIVE_FRIENDS],
                                achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_FIVE_FRIENDS, 1));
                    }
                }
            }
}
public void tenFriends(){
        if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_TEN_FRIENDS)).exists()){
            if(dataSnapshot.child(userId).child(SettingsConstants.FRIENDS).exists()){
                if(dataSnapshot.child(userId).child(SettingsConstants.FRIENDS).getChildrenCount() >= 10 ){
                    myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_TEN_FRIENDS)).setValue("1");
                    notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_TEN_FRIENDS],
                            achievement_description[SettingsConstants.ACHIEVEMENT_TEN_FRIENDS],
                            achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_TEN_FRIENDS, 1));
                }
            }
        }
    }

public void firstFollow(){
        if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_FOLLOW)).exists()){
            if(dataSnapshot.child(userId).child(SettingsConstants.FOLLOWERS).exists()){
                if(dataSnapshot.child(userId).child(SettingsConstants.FOLLOWERS).getChildrenCount() >= 1){
                    myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_FOLLOW)).setValue("1");
                    notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_FIRST_FOLLOW],
                            achievement_description[SettingsConstants.ACHIEVEMENT_FIRST_FOLLOW],
                            achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_FIRST_FOLLOW, 1));
                }
            }
        }
        fiftyFollow();
        OneHungFollow();
    }
public void fiftyFollow(){
        if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIFTY_FOLLOW)).exists()){
            if(dataSnapshot.child(userId).child(SettingsConstants.FOLLOWERS).exists()){
                if(dataSnapshot.child(userId).child(SettingsConstants.FOLLOWERS).getChildrenCount() >= 50){
                    myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIFTY_FOLLOW)).setValue("1");
                    notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_FIFTY_FOLLOW],
                            achievement_description[SettingsConstants.ACHIEVEMENT_FIFTY_FOLLOW],
                            achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_FIFTY_FOLLOW, 1));
                }
            }
        }
    }
public void OneHungFollow(){
        if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_ONEHUNG_FOLLOW)).exists()){
            if(dataSnapshot.child(userId).child(SettingsConstants.FOLLOWERS).exists()){
                if(dataSnapshot.child(userId).child(SettingsConstants.FOLLOWERS).getChildrenCount() >= 100){
                    myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_ONEHUNG_FOLLOW)).setValue("1");
                    notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_ONEHUNG_FOLLOW],
                            achievement_description[SettingsConstants.ACHIEVEMENT_ONEHUNG_FOLLOW],
                            achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_ONEHUNG_FOLLOW, 1));
                }
            }
        }
    }

public void tenTrain(){
        if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_TEN_TRAIN)).exists()){
            if(dataSnapshot.child(userId).child(SettingsConstants.TRAINING).exists()){
                if(dataSnapshot.child(userId).child(SettingsConstants.TRAINING).getChildrenCount() >= 10){
                    myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_TEN_TRAIN)).setValue("1");
                    notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_TEN_TRAIN],
                            achievement_description[SettingsConstants.ACHIEVEMENT_TEN_TRAIN],
                            achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_TEN_TRAIN, 1));
                }}}
   fiftyTrain();
   oneHundredTrain();
    }
public void fiftyTrain(){
        if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIFTY_TRAIN)).exists()){
            if(dataSnapshot.child(userId).child(SettingsConstants.TRAINING).exists()){
                if(dataSnapshot.child(userId).child(SettingsConstants.TRAINING).getChildrenCount() >= 50){
                    myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIFTY_TRAIN)).setValue("1");
                    notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_FIFTY_TRAIN],
                            achievement_description[SettingsConstants.ACHIEVEMENT_FIFTY_TRAIN],
                            achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_FIFTY_TRAIN, 1));
                }}}
    }
public void oneHundredTrain(){
        if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_ONE_HUNDRED_TRAIN)).exists()){
            if(dataSnapshot.child(userId).child(SettingsConstants.TRAINING).exists()){
                if(dataSnapshot.child(userId).child(SettingsConstants.TRAINING).getChildrenCount() >= 100){
                    myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_ONE_HUNDRED_TRAIN)).setValue("1");
                    notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_ONE_HUNDRED_TRAIN],
                            achievement_description[SettingsConstants.ACHIEVEMENT_ONE_HUNDRED_TRAIN],
                            achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_ONE_HUNDRED_TRAIN, 1));
                }}}
    }

public void morningTrain() {
        if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_MORNING_TRAIN)).exists()){
            Calendar DataStatic = Calendar.getInstance();
            int hour = DataStatic.getTime().getHours();
            if (hour < 11 && hour >6) {
                myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_MORNING_TRAIN)).setValue("1");
                notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_MORNING_TRAIN],
                        achievement_description[SettingsConstants.ACHIEVEMENT_MORNING_TRAIN],
                        achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_MORNING_TRAIN, 1));
            }
        }
    }
public void eveningTrain(){
        if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_EVENING_TRAIN)).exists()){
            Calendar DataStatic = Calendar.getInstance();
            int hour = DataStatic.getTime().getHours();
            if (hour < 23 && hour >18) {
                myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_EVENING_TRAIN)).setValue("1");
                notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_EVENING_TRAIN],
                        achievement_description[SettingsConstants.ACHIEVEMENT_EVENING_TRAIN],
                        achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_EVENING_TRAIN, 1));
            }
        }
    }
public void greatMonth(){
        if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_GREAT_MONTH)).exists()){
            myRef.child(userId).child(SettingsConstants.TRAINING).limitToLast(10).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.getChildrenCount() >= 10 ){
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            String tempData = postSnapshot.child(SettingsConstants.DATA).getValue().toString();
                            int tempIntData = Integer.parseInt(tempData.substring(3,5));
                            Calendar c = Calendar.getInstance();
                            int tempCurrentData = c.get(Calendar.MONTH);

                            if(tempIntData == tempCurrentData+1){
                                myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshott) {
                                        myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_GREAT_MONTH)).setValue("1");
                                        notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_GREAT_MONTH],
                                                achievement_description[SettingsConstants.ACHIEVEMENT_GREAT_MONTH],
                                                achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_GREAT_MONTH, 1));

                                    }@Override public void onCancelled(DatabaseError databaseError) {}});
                                break;
                            }else{
                                break;
                            }
                        }}}@Override public void onCancelled(DatabaseError databaseError) {}});
        }
    }

public void firstTonnage(float tonnage){
        if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_TONNAGE)).exists()){
            if(tonnage > 7000){
                myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_TONNAGE)).setValue("1");
                notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_FIRST_TONNAGE],
                        achievement_description[SettingsConstants.ACHIEVEMENT_FIRST_TONNAGE],
                        achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_FIRST_TONNAGE, 1));
            }
        }
   twoTonnage(tonnage);
   threeTonnage(tonnage);
    }
public void twoTonnage(float tonnage){
    if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_TWO_TONNAGE)).exists()){
        if(tonnage > 12000){
                        myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_TWO_TONNAGE)).setValue("1");
                        notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_TWO_TONNAGE],
                                achievement_description[SettingsConstants.ACHIEVEMENT_TWO_TONNAGE],
                                achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_TWO_TONNAGE, 1));
                    }
    }
}

public void threeTonnage(float tonnage){
    if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_THREE_TONNAGE)).exists()){
        if(tonnage > 18000) {
                        myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_THREE_TONNAGE)).setValue("1");
                        notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_THREE_TONNAGE],
                                achievement_description[SettingsConstants.ACHIEVEMENT_THREE_TONNAGE],
                                achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_THREE_TONNAGE, 1));
                }
    }
}

public void firstTonnageFull(float tonnage){
        if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_TONNAGE_FULL)).exists()){
            if(tonnage > 250000){
                myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_TONNAGE_FULL)).setValue("1");
                notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_FIRST_TONNAGE_FULL],
                        achievement_description[SettingsConstants.ACHIEVEMENT_FIRST_TONNAGE_FULL],
                        achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_FIRST_TONNAGE_FULL, 1));
            }
        }
    twoTonnageFull(tonnage);
    threeTonnageFull(tonnage);
    }
public void twoTonnageFull(float tonnage){
        if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_TWO_TONNAGE_FULL)).exists()){
            if(tonnage > 750000){
                myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_TWO_TONNAGE_FULL)).setValue("1");
                notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_TWO_TONNAGE_FULL],
                        achievement_description[SettingsConstants.ACHIEVEMENT_TWO_TONNAGE_FULL],
                        achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_TWO_TONNAGE_FULL, 1));
            }
        }
    }
public void threeTonnageFull(float tonnage){
        if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_THREE_TONNAGE_FULL)).exists()){
            if(tonnage > 1000000) {
                myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_THREE_TONNAGE_FULL)).setValue("1");
                notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_THREE_TONNAGE_FULL],
                        achievement_description[SettingsConstants.ACHIEVEMENT_THREE_TONNAGE_FULL],
                        achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_THREE_TONNAGE_FULL, 1));
            }
        }
    }

public void firstKpw(int kpw){
    if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_KPW)).exists()){
        if(kpw> 150){
                        myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_KPW)).setValue("1");
                        notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_FIRST_KPW],
                                achievement_description[SettingsConstants.ACHIEVEMENT_FIRST_KPW],
                                achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_FIRST_KPW, 1));
                    }
    }
    twoKpw(kpw);
    threeKpw(kpw);
}
public void twoKpw(int kpw){
    if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_TWO_KPW)).exists()){
        if(kpw> 250) {
                        myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_TWO_KPW)).setValue("1");
                        notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_TWO_KPW],
                                achievement_description[SettingsConstants.ACHIEVEMENT_TWO_KPW],
                                achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_TWO_KPW, 1));
                    }
    }
}
public void threeKpw(int kpw){
    if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_THREE_KPW)).exists()){
        if(kpw> 400) {
                        myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_THREE_KPW)).setValue("1");
                        notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_THREE_KPW],
                                achievement_description[SettingsConstants.ACHIEVEMENT_THREE_KPW],
                                achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_THREE_KPW, 1));
                    }
    }
}

public void firstKpwFull(int kpw){
    if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_KPW_FULL)).exists()){
        if(kpw> 2500){
                        myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_FIRST_KPW_FULL)).setValue("1");
                        notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_FIRST_KPW_FULL],
                                achievement_description[SettingsConstants.ACHIEVEMENT_FIRST_KPW_FULL],
                                achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_FIRST_KPW_FULL, 1));
                    }
    }
    twoKpwFull(kpw);
    threeKpwFull(kpw);
}
public void twoKpwFull(int kpw){
    if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_TWO_KPW_FULL)).exists()){
        if(kpw> 7500) {
                        myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_TWO_KPW_FULL)).setValue("1");
                        notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_TWO_KPW_FULL],
                                achievement_description[SettingsConstants.ACHIEVEMENT_TWO_KPW_FULL],
                                achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_TWO_KPW_FULL, 1));
                    }
    }
}
public void threeKpwFull(int kpw){
    if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_THREE_KPW_FULL)).exists()){
        if(kpw> 10000) {
                        myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_THREE_KPW_FULL)).setValue("1");
                        notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_THREE_KPW_FULL],
                                achievement_description[SettingsConstants.ACHIEVEMENT_THREE_KPW_FULL],
                                achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_THREE_KPW_FULL, 1));
                    }
    }
}

public void trainer() {
    if(!dataSnapshot.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_TRAINER)).exists()){
        if(dataSnapshot.child(userId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.QUALIFICATION).exists()){
        if(dataSnapshot.child(userId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.QUALIFICATION).equals("2")){
                myRef.child(userId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(SettingsConstants.ACHIEVEMENT_TRAINER)).setValue("1");
                notifycationToDevice(achievement_name[SettingsConstants.ACHIEVEMENT_TRAINER],
                        achievement_description[SettingsConstants.ACHIEVEMENT_TRAINER],
                        achievement_image.getResourceId(SettingsConstants.ACHIEVEMENT_TRAINER, 1));
            }
        }
    }
}

    private void notifycationToDevice(String title, String description, int  icon) {
        Notification.Builder builder = new Notification.Builder(context);
        builder
                .setSmallIcon(icon)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), icon))
                .setTicker(context.getString(R.string.get_achiewement))
                .setWhen(System.currentTimeMillis())
                .setShowWhen(true)
                .setAutoCancel(true)
                .setContentTitle(context.getString(R.string.gey_achiewemet_titlee));

        messages.add(title);

        Notification.InboxStyle inbox = new Notification.InboxStyle(builder);
        for (String m : messages) {
                inbox.addLine(m);
            }

        int defaults = 0;
        defaults |= Notification.DEFAULT_VIBRATE;
        defaults |= Notification.DEFAULT_SOUND;
        builder.setDefaults(defaults);
        Notification nc = inbox.build();
        NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (nm != null) {
            nm.notify(10, nc);
        }
    }

}
