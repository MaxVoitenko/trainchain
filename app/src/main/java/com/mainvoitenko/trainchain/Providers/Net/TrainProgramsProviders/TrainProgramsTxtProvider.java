package com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders;


import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mainvoitenko.trainchain.Model.TrainPrograms;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import static android.os.Environment.DIRECTORY_DOCUMENTS;

public class TrainProgramsTxtProvider {

    private Callback callback;
    public interface Callback{ void callingBackTrainProgramsTxt(StringBuilder stringBuilder);}
    public void registerCallBack(Callback callback){ this.callback = callback; }

    private Context context;

    public TrainProgramsTxtProvider(Context context) {
        this.context = context;
    }

    public void loadTxtProgram(String id) {

        StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        final File file;
        file= new File(context.getExternalFilesDir(DIRECTORY_DOCUMENTS)+ File.separator + "trainPrograms");
        if(!file.exists()){
            file.mkdirs();
        }
        final File dir = new File(file, id + ".txt");
        StringBuilder stringBuilder = null;
        try {
            stringBuilder = getTextFromDir(dir);
        }finally {
            if(stringBuilder==null){
                mStorageRef.child(SettingsConstants.TRAINING_PROGRAMS).child(id+".txt").getFile(dir)
                        .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                callback.callingBackTrainProgramsTxt(getTextFromDir(dir));
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(context, R.string.error_toast_load, Toast.LENGTH_SHORT).show();
                    }
                });}else{
            callback.callingBackTrainProgramsTxt(stringBuilder);
            }
        }
    }


    private StringBuilder getTextFromDir(File dir){
        if (dir.exists()){
            BufferedReader br = null;
            StringBuilder stringBuilder = new StringBuilder();
            try {
                br = new BufferedReader(new FileReader(dir));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            String str;
            try {
                while ((str = br.readLine()) != null) {
                    stringBuilder.append(str);
                    stringBuilder.append("\n");
                }
                return stringBuilder;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }else{
            return null;}
    }
}