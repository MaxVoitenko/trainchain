package com.mainvoitenko.trainchain.Providers.Net.AchievementsProvider;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Model.AchievementEqual;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

public class FriendAchievementProvider implements ValueEventListener {

    private Callback callback;
    public interface Callback{ void callingBackAchievementsFriend(ArrayList<ItemGrouper> allAchievements);}
    public void registerCallBack(Callback callback){ this.callback = callback; }

    private String myId;
    private String personId;
    private Context context;

    public FriendAchievementProvider(String myId, String personId, Context context) {
        this.myId = myId;
        this.personId = personId;
        this.context = context;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<ItemGrouper> achievements = new ArrayList<>();
        String[] achievement_name = context.getResources().getStringArray(R.array.achievement_name); //имена
        String[] achievement_description = context.getResources().getStringArray(R.array.achievement_description); //описания
        TypedArray achievement_image = context.getResources().obtainTypedArray(R.array.achievement_drawble);//картинки
        int temp = 0;
        for (int i = 0; i < achievement_name.length; i++) {
            if (dataSnapshot.child(personId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(i)).exists()) {
                temp = SettingsConstants.ACHIEVEMENT_IS_DONE;
            } else {
                temp = SettingsConstants.ACHIEVEMENT_IS_NOT_DONE;
            }
            if (dataSnapshot.child(myId).child(SettingsConstants.ACHIEVEMENT).child(String.valueOf(i)).exists()) {
                achievements.add(new AchievementEqual(achievement_name[i], achievement_description[i], achievement_image.getDrawable(i), SettingsConstants.ACHIEVEMENT_IS_DONE, temp, SettingsConstants.ACHIEVEMENT_IS_DONE));
            } else {
                achievements.add(new AchievementEqual(achievement_name[i], achievement_description[i], achievement_image.getDrawable(i), SettingsConstants.ACHIEVEMENT_IS_NOT_DONE, temp, SettingsConstants.ACHIEVEMENT_IS_DONE));
            }
        }
        callback.callingBackAchievementsFriend(achievements);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
}
