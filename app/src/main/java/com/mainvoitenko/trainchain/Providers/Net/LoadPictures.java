package com.mainvoitenko.trainchain.Providers.Net;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;

import static android.os.Environment.DIRECTORY_PICTURES;
import static java.io.File.createTempFile;


public class LoadPictures {
    private StorageReference mStorageRef;

    private Context context;
    private ImageView imageView;
    private ProgressBar progress_pic;

    public LoadPictures(StorageReference mStorageRef, Context context, ImageView imageView, ProgressBar progress_pic) {
        this.mStorageRef = mStorageRef;
        this.context = context;
        this.imageView = imageView;
        this.progress_pic = progress_pic;
    }

    public LoadPictures(StorageReference mStorageRef, Context context, ImageView imageView){
        this(mStorageRef, context, imageView, null);
    }


    public void addFullPicToPrifile(String prefix, String userId){
        if (progress_pic!=null){
        progress_pic.setVisibility(View.VISIBLE);}


        File temp = new File(context.getExternalFilesDir(DIRECTORY_PICTURES)+ File.separator + "photoAccount");
        if(!temp.exists()){
            temp.mkdirs();
        }
        final File dir = new File(temp, userId + ".jpg");
        if(!getPicFromDir(dir)){
        mStorageRef.child(prefix).child(userId).getFile(dir)
                .addOnSuccessListener(taskSnapshot -> {
                    if (isValidContextForGlideActivity(context)) {
                           Picasso.with(context)
                                .load(Uri.fromFile(dir))
                                   .memoryPolicy(MemoryPolicy.NO_CACHE)
                                   .memoryPolicy(MemoryPolicy.NO_STORE)
                                   .networkPolicy(NetworkPolicy.NO_CACHE)
                                   .networkPolicy(NetworkPolicy.NO_STORE)
                                .into(imageView);
                        getPicFromDir(dir);
                    }
                }).addOnFailureListener(e -> {
                    if(progress_pic !=null){
                        progress_pic.setVisibility(View.GONE);}
                });
        }
    }


public static boolean isValidContextForGlideActivity(final Context context) {
        if (context == null) {
            return false;
        }
        if (context instanceof Activity) {
            final Activity activity = (Activity) context;
            if (activity.isDestroyed() || activity.isFinishing()) {
                return false;
            }
        }
        return true;
    }

private boolean getPicFromDir(File dir){
    if (dir.exists()){
        Picasso.with(context)
                .load(Uri.fromFile(dir))
                .into(imageView);
        if(progress_pic!=null){
            progress_pic.setVisibility(View.GONE);}
    return true;
    }else return false;
}
}
