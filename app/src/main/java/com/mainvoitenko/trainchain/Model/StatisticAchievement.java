package com.mainvoitenko.trainchain.Model;

import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

public class StatisticAchievement implements ItemGrouper {
private  int isSucc;
private int count;

    public StatisticAchievement(int isSucc, int count) {
        this.isSucc = isSucc;
        this.count = count;
    }

    public int getIsSucc() {
        return isSucc;
    }

    public int getCount() {
        return count;
    }

    @Override
    public int getItemType() {
        return SettingsConstants.GROUPER_STATISTIC_ACHIEMENT;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public int ShowButtonMenu() {
        return 0;
    }
}
