package com.mainvoitenko.trainchain.Model;

import android.graphics.drawable.Drawable;


public class ImageCardio {
    private String name;
    private Drawable resources;

    public ImageCardio(String name, Drawable resources) {
        this.name = name;
        this.resources = resources;
    }

    public String getName() {
        return name;
    }

    public Drawable getImageResources() {
        return resources;
    }
}

