package com.mainvoitenko.trainchain.Model;

import android.graphics.drawable.Drawable;

public class Achievements {

    private String name;
    private String description;
    private Drawable resources;
    private int type;

    public Achievements(String name, String description, Drawable resources, int type) {
        this.name = name;
        this.description = description;
        this.resources = resources;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Drawable getResources() {
        return resources;
    }

    public int getType() {return type;}
}
