package com.mainvoitenko.trainchain.Training.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mainvoitenko.trainchain.Training.ApiNet.PostsProvider;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterPost;
import com.mainvoitenko.trainchain.Providers.RemoveCacheData;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Interface.Clickers.OnUniversalClick;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PreviewMyTrainings  extends AppCompatActivity implements
                                                OnUniversalClick,
                                                SwipeRefreshLayout.OnRefreshListener,
                                                PostsProvider.Callback {

    @BindView(R.id.recyclerView) RecyclerView rvFriend;
    @BindView(R.id.progress_pic) ProgressBar progress_pic;
    @BindView(R.id.clickBack) AppBarLayout clickBack;
    @BindView(R.id.swipe_container) SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.splash_no_train) ImageView splash_no_train;

    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();

    private ArrayList<ItemGrouper> items = new ArrayList<>();
    private RvAdapterPost recyclerAdapterForPost;
    private StorageReference mStorageRef;
    private String personId;
    private int countItems = 0;
    private PostsProvider postsProvider;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activivty_friend_achievements);
        ButterKnife.bind(this);

        clickBack.setVisibility(View.VISIBLE);
        myRef.keepSynced(true);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        Intent intent = getIntent();
        personId = intent.getStringExtra(SettingsConstants.PERSON_ID_INTENT);
        mStorageRef = FirebaseStorage.getInstance().getReference();

        recyclerAdapterForPost = new RvAdapterPost(items, this, false);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        rvFriend.setHasFixedSize(true);
        recyclerAdapterForPost.setOnUniversalClick(this);
        rvFriend.setLayoutManager(mLayoutManager);
        rvFriend.setAdapter(recyclerAdapterForPost);

        updateData();
    }

    @OnClick(R.id.clickBack)
    public void clickBackClick(View v) {
        finish();
    }

    private void updateData() {
        countItems = countItems + 10;
        progress_pic.setVisibility(View.VISIBLE);

        postsProvider = new PostsProvider(mStorageRef, SettingsConstants.POST_WITH_BUTTON_MENU, user.getUid());
        postsProvider.registerCallBack(this);
        myRef.child(user.getUid()).child(SettingsConstants.TRAINING).limitToLast(countItems).addListenerForSingleValueEvent(postsProvider);
    }

    @Override
    public void onUniversalClickListener() {
        updateData();
    }

    @Override
    public void onRefresh() {
        RemoveCacheData removeCacheData = new RemoveCacheData(this);
        removeCacheData.deleteOnePic(personId);
        countItems=0;
        updateData();
    }

    @Override
    public void callingBackPosts(ArrayList<ItemGrouper> posts) {
        if (posts != null) {
            if (posts.size() == 0) {
                splash_no_train.setVisibility(View.VISIBLE);
            } else {
                splash_no_train.setVisibility(View.GONE);
                items.clear();
                items.addAll(posts);
                progress_pic.setVisibility(View.GONE);
                if (items.size() == countItems) {
                    items.add(0, null);
                }
                mSwipeRefreshLayout.setRefreshing(false);
                recyclerAdapterForPost.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onDestroy() {
        myRef.removeEventListener(postsProvider);
        super.onDestroy();
    }
}
