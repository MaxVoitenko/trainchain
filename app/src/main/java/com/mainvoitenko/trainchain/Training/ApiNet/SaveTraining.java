package com.mainvoitenko.trainchain.Training.ApiNet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mainvoitenko.trainchain.Training.Entity.FullTraining;
import com.mainvoitenko.trainchain.Training.TrainingHelper.TrainingHelper;
import com.mainvoitenko.trainchain.Providers.RemoveCacheData;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

public class SaveTraining {

    private Context context;
    private ProgressBar progressBar;
    private StorageReference mStorageRef;


    public SaveTraining(Context context, ProgressBar progressBar, StorageReference mStorageRef) {
        this.context = context;
        this.progressBar = progressBar;
        this.mStorageRef = mStorageRef;
    }

    private void uploadPictureInFireBaseStorage(final Uri uri, final Context context, String idTrain) {
        UploadTask uploadTask = mStorageRef.child(SettingsConstants.TRAINING_PICTURES).child(idTrain).putFile(uri);
        uploadTask.addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred());
            RemoveCacheData removeCacheData = new RemoveCacheData(context);
            removeCacheData.deleteOnePic(String.valueOf(uri));
        }).addOnSuccessListener(taskSnapshot -> {
//                context.getContentResolver().delete(uri, null, null);
        });
    }

    public void saveTrainingToFirebase(FullTraining training, final DatabaseReference myRef,
                             final FirebaseUser user){
        DatabaseReference trainPush = myRef.child(user.getUid()).child(SettingsConstants.TRAINING).push(); //запушеная директория
        trainPush.setValue(training);
    }

    @SuppressLint("CheckResult")
    public void isEqualWithRoomTrain(ArrayList<ItemGrouper> realItems,
                                     Context context) {
        TrainingHelper.getTrainFromRoom(context)
                .subscribe(trainRoom -> {
                    if (realItems.size() > 0) {
                        if (realItems.get(realItems.size()-1) instanceof FullTraining) {
                            if (((FullTraining)realItems.get(realItems.size()-1)).getDataTraining().equals(trainRoom.getDataTraining())
                                    && ((FullTraining)realItems.get(realItems.size()-1)).getEmotion().equals(trainRoom.getEmotion())
                                    && ((FullTraining)realItems.get(realItems.size()-1)).getKpw().equals(trainRoom.getKpw())){
                                TrainingHelper.deleteTrainFromRoom(context);
                            }else{
                                Toast.makeText(context, "Есть не сохраненная тренировка", Toast.LENGTH_SHORT).show();//todo countExeercice
                                                                                                                          //todo  hard and card
                            }
                        }
                    }
                }, throwable -> {
                });
    }
}

