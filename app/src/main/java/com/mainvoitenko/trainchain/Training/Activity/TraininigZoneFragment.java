package com.mainvoitenko.trainchain.Training.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mainvoitenko.trainchain.Training.Entity.FullTraining;
import com.mainvoitenko.trainchain.Training.TrainingHelper.TrainingHelper;
import com.mainvoitenko.trainchain.Training.ApiNet.PostsProvider;
import com.mainvoitenko.trainchain.Training.ApiNet.SaveTraining;
import com.mainvoitenko.trainchain.Interface.Clickers.OnUniversalClick;
import com.mainvoitenko.trainchain.Interface.Clickers.OnPictureClickListener;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.UI.Activity.PhotoPostActivity;
import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterPost;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.R;

import java.util.ArrayList;


import static android.app.Activity.RESULT_OK;


public class TraininigZoneFragment extends Fragment  implements View.OnClickListener,
                                SwipeRefreshLayout.OnRefreshListener, OnUniversalClick,
                                OnPictureClickListener, PostsProvider.Callback {

    private ImageButton goTraininigButton;
    private Intent intent;
    private RecyclerView rvZone;
    private ArrayList<ItemGrouper> items;
    private ProgressBar progress_pic;
    private RvAdapterPost recyclerAdapterForPost;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private StorageReference mStorageRef;
    private ImageView splash_no_data;

    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();

    private int countItems =  0;

    private PostsProvider postsProvider;
    private SaveTraining saveTraining;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_traininig_zone, container,false);
        goTraininigButton = mView.findViewById(R.id.goTraininigButton);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipe_container);
        rvZone = mView.findViewById(R.id.rvZone);
        progress_pic = mView.findViewById(R.id.progress_pic);
        splash_no_data = mView.findViewById(R.id.splash_no_train);

        postsProvider = new PostsProvider(mStorageRef,SettingsConstants.POST_WITH_BUTTON_MENU, user.getUid());
        saveTraining = new SaveTraining(getContext(), progress_pic, mStorageRef);
        items = new ArrayList<>();
        mSwipeRefreshLayout.setOnRefreshListener(this);
        goTraininigButton.setOnClickListener(this);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        recyclerAdapterForPost = new RvAdapterPost(items, getContext());
        recyclerAdapterForPost.setOnUniversalClick(this);
        recyclerAdapterForPost.setOnPictureClickListener(this);
        myRef.keepSynced(true);

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        rvZone.setAdapter(recyclerAdapterForPost);
        rvZone.setLayoutManager(mLayoutManager);
        rvZone.setHasFixedSize(true);
        countItems =  0;
        updateData();

        return mView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.goTraininigButton:
                startActivityForResult(new Intent(getActivity(), ConstructorFirstPartActivity.class), 1);
                break;
        }
    }

    @SuppressLint("CheckResult")
    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == RESULT_OK && data!=null) {
            Bundle bundle = data.getExtras();
            int endTraining =  bundle.getInt(SettingsConstants.END_THIS_TRAIN_INTENT, 0);
            if (endTraining == 1) {
                FullTraining fullTraining = new FullTraining();
                TrainingHelper.getTrainFromRoom(getActivity().getApplicationContext()).subscribe(
                        trainingOtherDataHere -> {
                            TrainingHelper.updateAllData(fullTraining, trainingOtherDataHere);
                            saveTraining.saveTrainingToFirebase(trainingOtherDataHere, myRef, user);
                        }, throwable -> {});
        }
    }
}
    @Override
    public void onRefresh() {
        countItems=0;
        updateData();
    }

    @Override
    public void onUniversalClickListener() {
        updateData();
    }

    private void updateData(){
        countItems = countItems+10;
        progress_pic.setVisibility(View.VISIBLE);

        postsProvider.registerCallBack(this);
        myRef.child(user.getUid()).child(SettingsConstants.TRAINING).limitToLast(countItems).addListenerForSingleValueEvent(postsProvider);
    }

    @Override
    public void setOnPictureClickListener(String id, View v, int position) {
        if(id != null && !id.isEmpty()) {
            Intent intent = new Intent(getContext(), PhotoPostActivity.class);
            intent.putExtra(SettingsConstants.PHOTO_POST_INTENT, id);
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    getActivity(),
                    new Pair<View, String>(v.findViewById(R.id.picture_training),
                            getString(R.string.transition_name_circle)));
            ActivityCompat.startActivity(getActivity(), intent, options.toBundle());
        }
    }

    @Override
    public void callingBackPosts(ArrayList<ItemGrouper> posts) {
        if (posts != null) {
            if (posts.size() == 0) {
                splash_no_data.setVisibility(View.VISIBLE);
            } else {
                splash_no_data.setVisibility(View.GONE);
                items.clear();
                items.addAll(posts);
                if (items.size() == countItems) {
                    items.add(0, null);
                }
                    saveTraining.isEqualWithRoomTrain(items,getActivity().getApplicationContext());
            }
        }
        recyclerAdapterForPost.notifyDataSetChanged();
        progress_pic.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onDestroy() {
        myRef.removeEventListener(postsProvider);
        super.onDestroy();
    }
}