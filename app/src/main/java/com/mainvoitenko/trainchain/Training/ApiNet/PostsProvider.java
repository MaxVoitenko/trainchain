package com.mainvoitenko.trainchain.Training.ApiNet;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

public class PostsProvider implements ValueEventListener {

    private Callback callback;
    public interface Callback{ void callingBackPosts(ArrayList<ItemGrouper> posts);}
    public void registerCallBack(Callback callback){ this.callback = callback; }

    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
    private StorageReference mStorageRef;
    private int ShowMenu;
    private String id;

    public PostsProvider(StorageReference mStorageRef, int showMenu, String id) {
        this.mStorageRef = mStorageRef;
        ShowMenu = showMenu;
        this.id = id;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<ItemGrouper> items = new ArrayList<>();
        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
            if (!dataSnapshot.child(postSnapshot.getKey()).child(SettingsConstants.HARD_EX).exists() &&
                    dataSnapshot.child(postSnapshot.getKey()).child(SettingsConstants.CARDIO_EX).exists()) {
                items.add(new BaseTrainProvider(dataSnapshot, postSnapshot.getKey()).loadCardioTrain(ShowMenu));
            } else if (!dataSnapshot.child(postSnapshot.getKey()).child(SettingsConstants.CARDIO_EX).exists() &&
                    dataSnapshot.child(postSnapshot.getKey()).child(SettingsConstants.HARD_EX).exists()) {
                items.add(new BaseTrainProvider(dataSnapshot, postSnapshot.getKey()).loadHardTrain(ShowMenu));

            } else if (dataSnapshot.child(postSnapshot.getKey()).child(SettingsConstants.HARD_EX).exists() &&
                    dataSnapshot.child(postSnapshot.getKey()).child(SettingsConstants.CARDIO_EX).exists()) {
                items.add(new BaseTrainProvider(dataSnapshot, postSnapshot.getKey()).loadGroupTrain(ShowMenu));
            }else{
                myRef.child(id).child(SettingsConstants.TRAINING).child(postSnapshot.getKey()).removeValue();
                mStorageRef.child(SettingsConstants.TRAINING_PICTURES).child(postSnapshot.getKey()).delete();
            }
        }
        callback.callingBackPosts(items);
    }
    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
    }
}
