package com.mainvoitenko.trainchain.Training.ApiNet;

import com.google.firebase.database.DataSnapshot;
import com.mainvoitenko.trainchain.Training.Entity.FullTraining;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

public  class BaseTrainProvider{

    private DataSnapshot dataSnapshot;
    private String id;

    public BaseTrainProvider(DataSnapshot dataSnapshot, String id){
        this.dataSnapshot = dataSnapshot;
        this.id = id;
    }

    public FullTraining loadCardioTrain(int showMenu){
        FullTraining fullTraining = dataSnapshot.child(id).getValue(FullTraining.class);
        fullTraining.setInternetId(id);
        fullTraining.setShowMenu(showMenu);
        fullTraining.setItemType(SettingsConstants.GROUPER_CARDIO_TRAIN_POST);
        return fullTraining;
    }

    public FullTraining loadHardTrain(int showMenu){
        FullTraining fullTraining = dataSnapshot.child(id).getValue(FullTraining.class);
        fullTraining.setInternetId(id);
        fullTraining.setShowMenu(showMenu);
        fullTraining.setItemType(SettingsConstants.GROUPER_HARD_TRAIN_POST);
        return fullTraining;
    }
    public FullTraining loadGroupTrain(int showMenu){
        FullTraining fullTraining = dataSnapshot.child(id).getValue(FullTraining.class);
        fullTraining.setInternetId(id);
        fullTraining.setShowMenu(showMenu);
        fullTraining.setItemType(SettingsConstants.GROUPER_GROOP_TRAIN_POST);
        return fullTraining;
    }
  }
