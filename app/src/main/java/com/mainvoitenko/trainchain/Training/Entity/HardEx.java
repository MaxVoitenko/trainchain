package com.mainvoitenko.trainchain.Training.Entity;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.database.ThrowOnExtraProperties;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.List;

@IgnoreExtraProperties
public class HardEx {

    @PropertyName("Name")
    public String nameExercise;
    @PropertyName("Rest")
    public String timerExercise;
    @PropertyName("Count")
    public List<Integer> myExerciseCount;
    @PropertyName("Weight")
    public List<Float> myExerciseWeght;
    @Exclude
    private float tonnage;
    @Exclude
    private int kpw;

    public HardEx(String nameExercise, List<Float> myExWeight, List<Integer> myExerciseCont, String timerExercise){
        this.myExerciseCount = myExerciseCont;
        this.myExerciseWeght = myExWeight;
        this.nameExercise = nameExercise;
        this.timerExercise = timerExercise;
    }
    public HardEx(){}

    @Exclude
    public List<Integer> getMyExerciseCount() {
        return myExerciseCount;
    }
    @Exclude
    public void setMyExerciseCount(List<Integer> myExerciseCount) {
        this.myExerciseCount = myExerciseCount;
    }
    @Exclude
    public List<Float> getMyExerciseWeght() {
        return myExerciseWeght;
    }
    @Exclude
    public void setMyExerciseWeght(List<Float> myExerciseWeght) {
        this.myExerciseWeght = myExerciseWeght;
    }
    @Exclude
    public String getNameExercise() {return nameExercise;}
    @Exclude
    public void setNameExercise(String nameExercise) {this.nameExercise = nameExercise;}
    @Exclude
    public void setCountExercise(List<Integer> myExerciseCount) { this.myExerciseCount = myExerciseCount; }
    @Exclude
    public List<Integer> getCountExercise() {return myExerciseCount; }
    @Exclude
    public void setWeghtExercise(List<Float> myExerciseWeght) {
        this.myExerciseWeght = myExerciseWeght;
    }
    @Exclude
    public List<Float> getWeghtExercise() {return myExerciseWeght; }
    @Exclude
    public String getTimerExercise() {return timerExercise;}
    @Exclude
    public void setTimerExercise(String timerExercise) {this.timerExercise = timerExercise;}
    @Exclude
    public float getTonnageExercise() {
        tonnage=0;
        for (int i = 0; i< SettingsConstants.COUNT_FOR_HARD_EXERCISE; i++){
            tonnage= tonnage + (myExerciseCount.get(i)* myExerciseWeght.get(i));
        }
        return tonnage;
    }
    @Exclude
    public int getKpwExercise() {
        kpw=0;
        for (int i=0; i<SettingsConstants.COUNT_FOR_HARD_EXERCISE; i++){
            kpw = kpw +  myExerciseCount.get(i);
        }
        return kpw;
    }

}

