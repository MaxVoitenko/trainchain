package com.mainvoitenko.trainchain.Account.ApiNet;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

public class RemoveAccountProvider implements ValueEventListener {

    private Callback callback;
    public interface Callback{ void callingBackRemoveAcc();}
    public void registerCallBack(Callback callback){ this.callback = callback; }

    private FirebaseUser user;
    private DatabaseReference myRef;
    private StorageReference mStorageRef;

    public RemoveAccountProvider(FirebaseUser user, DatabaseReference myRef, StorageReference mStorageRef) {
        this.user = user;
        this.myRef = myRef;
        this.mStorageRef = mStorageRef;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
            mStorageRef.child(SettingsConstants.TRAINING_PICTURES).child(postSnapshot.getKey()).delete();
        }
        mStorageRef.child(user.getUid()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                myRef.child(user.getUid()).removeValue();
                user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        callback.callingBackRemoveAcc();
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                myRef.child(user.getUid()).removeValue();
                user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        callback.callingBackRemoveAcc();
                    }
                });
            }
        });
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) { }
}
