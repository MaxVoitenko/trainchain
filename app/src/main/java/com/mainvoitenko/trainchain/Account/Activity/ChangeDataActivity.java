package com.mainvoitenko.trainchain.Account.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.mainvoitenko.trainchain.Account.Entity.AccountMy;
//import com.mainvoitenko.trainchain.Engine.LoadFireBaseEngine.LoadMyAccount;
import com.mainvoitenko.trainchain.Account.ApiNet.MyAccountProvider;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;

import java.net.MalformedURLException;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangeDataActivity extends AppCompatActivity implements MyAccountProvider.Callback {

     @BindView(R.id.newCity) EditText newCity;
     @BindView(R.id.newGym) EditText newGym;
     @BindView(R.id.newWeight) EditText newWeight;
     @BindView(R.id.newHeight) EditText newHeight;
     @BindView(R.id.newUrl) EditText newUrl;
     @BindView(R.id.urlBack) TextInputLayout urlBack;
     @BindView(R.id.name) EditText name;
     @BindView(R.id.progress_pic) ProgressBar progress_pic;
     @BindView(R.id.textView6) TextView textView6;

    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();

    private MyAccountProvider myAccountProvider;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_data_account);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progress_pic.setVisibility(View.VISIBLE);

        myAccountProvider = new MyAccountProvider(user.getUid(), this);
        myAccountProvider.registerCallBack(this);
        myRef.addListenerForSingleValueEvent(myAccountProvider);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.okay_change_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        progress_pic.setVisibility(View.VISIBLE);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                if(newUrl.getText().toString().length()==0){
                    newUrl.setText("");
                }else if(newUrl.getText().toString().length()<8){
                    String s = newUrl.getText().toString();
                    newUrl.setText("http://" + s);
                }else if (!String.valueOf(newUrl.getText()).substring(0,7).contains("http://")
                        & !String.valueOf(newUrl.getText()).substring(0,8).contains("https://")) {
                    String s = newUrl.getText().toString();
                    newUrl.setText("http://" + s);
                }
                final String[] allNewData = new String[6];

                allNewData[0] = String.valueOf(name.getText());
                allNewData[1] = String.valueOf(newCity.getText());
                allNewData[2] = String.valueOf(newGym.getText());

                allNewData[3] = String.valueOf(newWeight.getText());
                allNewData[4] = String.valueOf(newHeight.getText());
                allNewData[5] = String.valueOf(newUrl.getText());

                if (!allNewData[0].equals("")) {
                    DatabaseReference  tempRef = myRef.child(user.getUid());

                    tempRef.runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {//todo here
                        mutableData.child(SettingsConstants.NAME).setValue(allNewData[0]);
                        mutableData.child(SettingsConstants.ACCOUNT).child(SettingsConstants.CITY).setValue(allNewData[1]);
                        mutableData.child(SettingsConstants.ACCOUNT).child(SettingsConstants.GYM).setValue(allNewData[2]);
                        mutableData.child(SettingsConstants.ACCOUNT).child(SettingsConstants.WEIGHT).setValue(allNewData[3]);
                        mutableData.child(SettingsConstants.ACCOUNT).child(SettingsConstants.HEIGHT).setValue(allNewData[4]);
                        if(urlBack.getVisibility()== View.VISIBLE){
                            mutableData.child(SettingsConstants.ACCOUNT).child(SettingsConstants.MY_URL_ADRESS).setValue(allNewData[5]);
                        }
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                        progress_pic.setVisibility(View.GONE);
                        finish();
                    }
                });
                } else {
                    Toast.makeText(this, R.string.too_little_name, Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }
    private boolean isValid(String urlString) {
        try {
            URL url = new URL(urlString);
            return URLUtil.isValidUrl(urlString) && Patterns.WEB_URL.matcher(urlString).matches();
        } catch (MalformedURLException e) {}
        return false;
    }

    @Override
    public void callingBack(AccountMy accountMy, int qualification) {
        newCity.setText(accountMy.getCity());
        newGym.setText(accountMy.getGym());
        newWeight.setText(accountMy.getWeight());
        newHeight.setText(accountMy.getHeight());
        name.setText(accountMy.getName());
        progress_pic.setVisibility(View.GONE);
        if (qualification ==2){
            urlBack.setVisibility(View.VISIBLE);
            textView6.setVisibility(View.VISIBLE);
            newUrl.setText(accountMy.getMyUrlAdress());
        }
    }

    @Override
    protected void onDestroy() {
        myRef.removeEventListener(myAccountProvider);
        super.onDestroy();
    }
}


