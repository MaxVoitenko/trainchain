package com.mainvoitenko.trainchain.Account.ApiNet;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.mainvoitenko.trainchain.Account.Entity.AccountFriend;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;


public class FriendAccountProvider implements ValueEventListener{

    private Callback callback;
    public interface Callback{ void callingBack(AccountFriend accountFriend, int friendOrNot, String myQual);}
    public void registerCallBack(Callback callback){ this.callback = callback; }

    private String personId;
    private String myId;

    public FriendAccountProvider(String personId, String myId) {
        this.personId = personId;
        this.myId = myId;
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        int friendOrNot=SettingsConstants.THIS_PERSON_IS_NOT_ADD;
        for (DataSnapshot postSnapshot : dataSnapshot.child(myId).child(SettingsConstants.FRIENDS).getChildren()) {
            if (postSnapshot.getKey().equals(personId)){
                friendOrNot=SettingsConstants.THIS_PERSON_ALREADY_ADD;
            }
        }
        AccountFriend accountFriend = getFriendAllData(dataSnapshot, friendOrNot);
        String myQual = loadMyEqual(dataSnapshot,myId);
            callback.callingBack(accountFriend,friendOrNot, myQual);
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    public AccountFriend getFriendAllData(DataSnapshot dataSnapshot, int friendOrNot){
        BaseAccountDataProvider baseAccountDataProvider = new BaseAccountDataProvider(personId,dataSnapshot);
        AccountFriend account = new AccountFriend();
        account.setFriendOrNot(friendOrNot);
        account.setName(baseAccountDataProvider.loadName());
        ArrayList temp = baseAccountDataProvider.loadCityAndGym();
        account.setCity(temp.get(0).toString());
        account.setGym(temp.get(1).toString());
        temp = baseAccountDataProvider.loadHeightAndWeight();
        account.setHeight(temp.get(0).toString());
        account.setWeight(temp.get(1).toString());
        account.setCountMyFollowers(baseAccountDataProvider.loadCountMyFollowers());
        account.setCountMyFriends(baseAccountDataProvider.loadCountMyFriends());
        account.setQualificationAccount(baseAccountDataProvider.loadQualAccount());
        account.setMyUrlAdress(baseAccountDataProvider.loadUrlAdress());
        account.setCountTrain(baseAccountDataProvider.loadTrainCount());
        account.setFriendOrNot(friendOrNot);
        account.setPersonId(personId);
        account.setQualificationAccount(baseAccountDataProvider.loadQualAccount());
        return account;
    }
    private String loadMyEqual(DataSnapshot dataSnapshot, String myId){
        BaseAccountDataProvider baseAccountDataProvider = new BaseAccountDataProvider(myId,dataSnapshot);
        return baseAccountDataProvider.loadQualAccount();
    }
}