package com.mainvoitenko.trainchain.UI.Adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mainvoitenko.trainchain.Interface.Clickers.OnDialogTrainProgramTwoClickListener;
import com.mainvoitenko.trainchain.Interface.Clickers.OnTrainingProgramClickListener;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderForDialogTrainProgram;

import java.util.ArrayList;

public class RvAdapterDialogTrainProgramsTwo extends RecyclerView.Adapter<HolderForDialogTrainProgram> {

    private ArrayList<String> items;
    private OnDialogTrainProgramTwoClickListener onDialogTrainProgramTwoClickListener;
    public void setOnDialogTrainProgramTwoClickListener(OnDialogTrainProgramTwoClickListener onDialogTrainProgramTwoClickListener){
        this.onDialogTrainProgramTwoClickListener = onDialogTrainProgramTwoClickListener;}

    public RvAdapterDialogTrainProgramsTwo(ArrayList<String> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public HolderForDialogTrainProgram onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder;
        holder = new HolderForDialogTrainProgram(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_dialog_program_train, parent, false));
        return (HolderForDialogTrainProgram) holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final HolderForDialogTrainProgram holder, int position) {
        holder.getTextView().setText(items.get(holder.getAdapterPosition()));
        holder.getTextView().setTextSize(12);
        holder.getTextView().setTextColor(Color.BLACK);
        holder.itemView.setPadding(4,4,4,4);

        holder.getTextView().setGravity(Gravity.LEFT);
        holder.getTextView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onDialogTrainProgramTwoClickListener != null) {
                    onDialogTrainProgramTwoClickListener.OnDialogTrainProgramTwo(items.get(holder.getAdapterPosition()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
