package com.mainvoitenko.trainchain.UI.Adapters;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderForSearch;
import com.mainvoitenko.trainchain.Account.Entity.AccountFriend;
import com.mainvoitenko.trainchain.Providers.Net.LoadPictures;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Interface.Clickers.OnSearchClickListener;
import com.mainvoitenko.trainchain.Interface.Clickers.OnUniversalClick;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.R;

import java.util.ArrayList;


public class RvAdapterSearch extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ItemGrouper> friends = new ArrayList();
    private View vv;
    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();


    private final int TYPE_ITEM_PERSON = 4;

    public static final int REQUEST_BACK_TO_SEARCH = 992;
    private final int TYPE_ITEM_MOREE = 1408;

    private OnUniversalClick onUniversalClick;
    private OnSearchClickListener onSearchClickListener;
    private StorageReference mStorageRef;


    public RvAdapterSearch(ArrayList<ItemGrouper> friends) {
        this.friends = friends;
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    public void setOnUniversalClick(OnUniversalClick onUniversalClick){this.onUniversalClick = onUniversalClick;}

    public void setOnSearchClickListener(OnSearchClickListener onSearchClickListener) {
        this.onSearchClickListener = onSearchClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case TYPE_ITEM_PERSON:
                holder = new HolderForSearch(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_search, parent, false));
                break;
           default:
                holder = new RvAdapterSearch.MoreItems(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_moreee, parent, false));
                break;}
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        int type = getItemViewType(holder.getAdapterPosition());
        switch (type){
            case TYPE_ITEM_PERSON:
                final HolderForSearch holderPerson = (HolderForSearch) holder;
                final AccountFriend friend = (AccountFriend) friends.get(holder.getAdapterPosition());
                holderPerson.getFriendName().setText(friend.getName());
                holderPerson.getFriendCity().setText(friend.getCity() + ", " + friend.getGym());
                holderPerson.getImageProfile().setImageDrawable(null);

                LoadPictures loadPictures = new LoadPictures(mStorageRef,
                        holderPerson.itemView.getContext(),
                        holderPerson.getImageProfile());
                loadPictures.addFullPicToPrifile(SettingsConstants.USER_PICTURES, friend.getPersonId());

                if (!friend.getName().equals(holder.itemView.getContext().getString(R.string.del_accunt_for_parse_check))) {
                    holderPerson.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (onSearchClickListener != null) {
                                onSearchClickListener.setOnSearchClickListener(friend.getPersonId()
                                                                ,holderPerson.getImageProfile()
                                                                ,holderPerson.getAdapterPosition());
                            }
                        }
                    });
                } else {
                    holderPerson.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            removeDelete(holderPerson, friend);
                        }});

                }
                if(friend.getQualificationAccount().equals("2")){
                    holderPerson.getQualificationTypeAccount().setVisibility(View.VISIBLE);
                }else{
                    holderPerson.getQualificationTypeAccount().setVisibility(View.GONE);
                }
                break;
                case TYPE_ITEM_MOREE:
                    MoreItems holderMore = (MoreItems) holder;

                    break;
        }

    }

    @Override
    public int getItemCount() {
        return friends.size();
    }

    @Override
    public int getItemViewType(int position) {
        // определяем какой тип в текущей позиции
        if(friends.get(position)!= null){
        int type = friends.get(position).getItemType();
        if (type == TYPE_ITEM_PERSON) return TYPE_ITEM_PERSON;
        else  return TYPE_ITEM_MOREE;
        }else{return TYPE_ITEM_MOREE;}
    }


    public class MoreItems extends RecyclerView.ViewHolder implements View.OnClickListener{
        public MoreItems(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onUniversalClick != null) {
                onUniversalClick.onUniversalClickListener();
            }
        }
    }

    public void removeDelete(final HolderForSearch holderPerson, final AccountFriend friend){
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(user.getUid()).child(SettingsConstants.FRIENDS).child(friend.getPersonId()).exists()) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(holderPerson.itemView.getContext());
                    final AlertDialog alertDialog = builder.create();
                    builder.setTitle(R.string.account_search_friend_del_title);
                    builder.setMessage(R.string.delete_account_friend_search);

                    builder.setPositiveButton(R.string.yes_dialog, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            myRef.child(user.getUid()).child(SettingsConstants.FRIENDS).child(friend.getPersonId()).removeValue();
                            friends.remove(holderPerson.getAdapterPosition());
                            notifyItemRemoved(holderPerson.getAdapterPosition());
                        }
                    });
                    builder.setNegativeButton(R.string.cancel_dialog,null);
                    builder.show();
                }}@Override public void onCancelled(DatabaseError databaseError) {}});
    }
}
