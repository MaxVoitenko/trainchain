package com.mainvoitenko.trainchain.UI.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mainvoitenko.trainchain.Account.ApiNet.RemoveAccountProvider;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;

public class SettingsFragment extends Fragment implements View.OnClickListener, RemoveAccountProvider.Callback {

    private LinearLayout deleteClick;
    private LinearLayout exitClick;
    private LinearLayout removeCacheTrain;

    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();
    private FirebaseAuth mAuth = SingleTonUser.getInstance().getmAuth();

    private StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_settings, container,false);

        deleteClick = mView.findViewById(R.id.deleteClick);
        exitClick = mView.findViewById(R.id.exitClick);
        removeCacheTrain = mView.findViewById(R.id.removeCacheTrain);
        deleteClick.setOnClickListener(this);
        exitClick.setOnClickListener(this);
        removeCacheTrain.setOnClickListener(this);

        return mView;
    }

    @Override
    public void onClick(View v) {
     switch (v.getId()) {
         case R.id.deleteClick:
             final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
             final AlertDialog alertDialog = builder.create();
             builder.setTitle(R.string.account_delete_dialog);
             builder.setMessage(R.string.acc_del_dialog);
             builder.setPositiveButton(R.string.yes_dialog, (dialog, which) -> {
                 final AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                 final AlertDialog alertDialog1 = builder1.create();
                 builder1.setTitle(R.string.account_delete_dialog);
                 builder1.setMessage(R.string.del_acc_two);
                 builder1.setPositiveButton(R.string.yes_dialog, (dialog1, which1) -> {
                     final AlertDialog.Builder builder11 = new AlertDialog.Builder(getContext());
                     final AlertDialog alertDialog11 = builder11.create();
                     builder11.setTitle(R.string.account_delete_dialog);
                     builder11.setMessage(R.string.delete_three_dialog);
                     builder11.setPositiveButton(R.string.yes_dialog, (dialog11, which11) -> deleteAccount(getContext()));
                     builder11.setNegativeButton(R.string.cancel_dialog, null);
                     builder11.show();
                 });
                 builder1.setNegativeButton(R.string.cancel_dialog, null);
                 builder1.show();
             });
             builder.setNegativeButton(R.string.cancel_dialog, null);
             builder.show();

             break;
         case R.id.exitClick: {
             Toast.makeText(getContext(), R.string.exit_from_account, Toast.LENGTH_SHORT).show();
//             SaveTraining saveTraining = new SaveTraining(getContext(), null, mStorageRef);
//             saveTraining.deleteCacheTrain();
             mAuth.getInstance().signOut();
             getActivity().finish();
         }
         break;
         case R.id.removeCacheTrain: {
//             rewrite();
         }
         break;
     }
    }

    private void deleteAccount(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.wait_fr));
        progressDialog.show();

        RemoveAccountProvider removeAccountProvider = new RemoveAccountProvider(user,myRef,mStorageRef);
        removeAccountProvider.registerCallBack(this);
        myRef.child(user.getUid()).child(SettingsConstants.TRAINING).addListenerForSingleValueEvent(removeAccountProvider);
    }

    @Override
    public void callingBackRemoveAcc() {
        progressDialog.dismiss();
        getActivity().finish();
    }

//        myRef.child(user.getUid()).child(SettingsConstants.TRAINING).addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//            }
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {}});

//    private void deleteAllAchievements() {
//    myRef.addListenerForSingleValueEvent(new ValueEventListener() {
//        @Override
//        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//            for(DataSnapshot postSnapshot: dataSnapshot.getChildren()){
//                if(!postSnapshot.getKey().equals(SettingsConstants.TRAINING_PROGRAMS)){
//                for(DataSnapshot achievSnapshot :postSnapshot.child(SettingsConstants.ACHIEVEMENT).getChildren()){
//                    if(!achievSnapshot.getKey().toString().equals("0") &&
//                            !achievSnapshot.getKey().toString().equals("1") &&
//                            !achievSnapshot.getKey().toString().equals("2")){
////                        Log.d("mLog", "PostSnap  ->   " + myRef.child(postSnapshot.getKey()).child(SettingsConstants.ACHIEVEMENT).child(achievSnapshot.getKey().toString()));
//                        myRef.child(postSnapshot.getKey()).child(SettingsConstants.ACHIEVEMENT).child(achievSnapshot.getKey()).removeValue();
//                    }
//                }
//            }}
//        }@Override public void onCancelled(@NonNull DatabaseError databaseError) {}});
//    }

//    public void rewrite(){
//        Toast.makeText(getContext(), "goo", Toast.LENGTH_SHORT).show();
//
//        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                for (DataSnapshot idSnapschot : dataSnapshot.getChildren()) {
//                    if (idSnapschot.getKey() != SettingsConstants.TRAINING_PROGRAMS) {
//                        for (DataSnapshot postSnapshoy : idSnapschot.child("Training").getChildren()) {
//                            if (postSnapshoy.child("HardEx").getValue() != null) {
//                                for (DataSnapshot hardSnap : postSnapshoy.child("HardEx").getChildren()) {
//                                    for (DataSnapshot someHardSnap : hardSnap.child("Count").getChildren()) {
//                                        myRef.child(idSnapschot.getKey())
//                                                .child("Training")
//                                                .child(postSnapshoy.getKey())
//                                                .child("HardEx")
//                                                .child(hardSnap.getKey())
//                                                .child("Count")
//                                                .child(someHardSnap.getKey())
//                                                .setValue(Integer.parseInt(someHardSnap.getValue().toString()));
//                                    }
//                                    for (DataSnapshot someHardWSnap : hardSnap.child("Weight").getChildren()) {
//                                        myRef.child(idSnapschot.getKey())
//                                                .child("Training")
//                                                .child(postSnapshoy.getKey())
//                                                .child("HardEx")
//                                                .child(hardSnap.getKey())
//                                                .child("Weight")
//                                                .child(someHardWSnap.getKey())
//                                                .setValue(Float.parseFloat(someHardWSnap.getValue().toString()));
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }
}
