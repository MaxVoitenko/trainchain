package com.mainvoitenko.trainchain.UI.Adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderAchiewement;
import com.mainvoitenko.trainchain.Model.Achievements;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

public class RvAdapterAchievement extends RecyclerView.Adapter<HolderAchiewement>{

        private View v;
        private ArrayList<Achievements> achievements = new ArrayList<>();


    public RvAdapterAchievement(ArrayList<Achievements> achievements) {
        this.achievements = achievements;
    }

    @Override
    public HolderAchiewement onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_achievement, parent, false);
        HolderAchiewement HolderAchiewement = new HolderAchiewement(v);
        return HolderAchiewement;
    }

    @Override
    public void onBindViewHolder(HolderAchiewement holder, int position) {
holder.getImageAchevement().setImageDrawable(achievements.get(holder.getAdapterPosition()).getResources());
holder.getNameAchevement().setText(achievements.get(holder.getAdapterPosition()).getName());
holder.getDescriptionAchevement().setText(achievements.get(holder.getAdapterPosition()).getDescription());


if(achievements.get(holder.getAdapterPosition()).getType() == SettingsConstants.ACHIEVEMENT_IS_DONE) {
    holder.getImageAchevement().setVisibility(View.VISIBLE);
    holder.getQuestionAchivement().setVisibility(View.GONE);
    holder.getNameAchevement().setTextColor(Color.YELLOW);
    holder.getNameAchevement().setTextSize(15);
    holder.getNameAchevement().setEnabled(true);
    holder.getDescriptionAchevement().setEnabled(true);
//    holder.getDescriptionAchevement().setVisibility(View.VISIBLE);
    holder.getNameAchevement().setShadowLayer(15, 0, 0, Color.BLACK);
}else{
//    holder.getDescriptionAchevement().setVisibility(View.GONE);
    holder.getNameAchevement().setEnabled(false);
    holder.getNameAchevement().setTextSize(15);
    holder.getDescriptionAchevement().setEnabled(false);
    holder.getImageAchevement().setVisibility(View.GONE);
    holder.getQuestionAchivement().setVisibility(View.VISIBLE);
    holder.getNameAchevement().setTextColor(holder.itemView.getResources().getColor(R.color.text_hard_gray));
    holder.getNameAchevement().setShadowLayer(0, 0, 0, Color.BLACK);
}
    }

    @Override
    public int getItemCount() {
        return achievements.size();
    }

}
