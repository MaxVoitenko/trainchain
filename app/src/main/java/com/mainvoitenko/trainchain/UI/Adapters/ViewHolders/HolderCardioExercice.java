package com.mainvoitenko.trainchain.UI.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mainvoitenko.trainchain.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HolderCardioExercice extends RecyclerView.ViewHolder {

    @BindView(R.id.nameForCardio) TextView nameForCardio;
    @BindView(R.id.timeForCardio) TextView timeForCardio;
    @BindView(R.id.deleteCardio) TextView deleteCardio;

    public HolderCardioExercice(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public TextView getNameForCardio() {
        return nameForCardio;
    }

    public TextView getTimeForCardio() {
        return timeForCardio;
    }

    public TextView getDeleteCardio() {
        return deleteCardio;
    }
}