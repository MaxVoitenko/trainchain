package com.mainvoitenko.trainchain.UI.Adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderForStatisticChart;
import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderForStatisticCircle;
import com.mainvoitenko.trainchain.Model.StatisticCircleChart;
import com.mainvoitenko.trainchain.Model.StatisticChart;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.razerdp.widget.animatedpieview.AnimatedPieViewConfig;
import com.razerdp.widget.animatedpieview.callback.OnPieSelectListener;
import com.razerdp.widget.animatedpieview.data.IPieInfo;
import com.razerdp.widget.animatedpieview.data.SimplePieInfo;

import java.util.ArrayList;


public class RvAdapterCharts extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int TYPE_ITEM_OTHER_DATA= SettingsConstants.GROUPER_STATISTIC_CHART;
    private final int TYPE_ITEM_CIRCLE=  SettingsConstants.GROUPER_STATISTIC_CIRCLE_CHART;

    private final int TONNAGE_CHART = 1;
    private final int KPW_CHART = 2;
    private final int INTENCITY_CHART = 3;
    private final int WEIGHT_CHART = 4;

    private ArrayList<ItemGrouper> data;

    public RvAdapterCharts(ArrayList<ItemGrouper> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder;
        switch (viewType) {
            case TYPE_ITEM_OTHER_DATA:
                holder = new HolderForStatisticChart(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_chart_other, parent, false));
                break;
            default:
                holder = new HolderForStatisticCircle(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_chart_circle, parent, false));
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(holder.getAdapterPosition());

         if (type == TYPE_ITEM_OTHER_DATA) {
            final HolderForStatisticChart holderForStatisticChart = (HolderForStatisticChart) holder;
            StatisticChart statisticChart = (StatisticChart) data.get(holder.getAdapterPosition());
            final LineDataSet lineDataSet = new LineDataSet(null, statisticChart.getName());
            ArrayList<ILineDataSet> dataSetstonnage = new ArrayList<>();
                dataSetstonnage.add(lineDataSet);
            if(dataSetstonnage.size()>0) {
            for(int i = 0; i< statisticChart.getData().size(); i++) {
                lineDataSet.addEntry(new Entry(i+1, statisticChart.getData().get(i)));
            }
            LineData lineData = new LineData(lineDataSet);
            holderForStatisticChart.getChart_line().setData(lineData);
            holderForStatisticChart.getChart_line().animateY(500);
            holderForStatisticChart.getChart_line().setVisibleXRangeMaximum(12);
            holderForStatisticChart.getChart_line().moveViewToX(lineDataSet.getXMax());
            updateDesignChart(holderForStatisticChart, lineDataSet);
                holderForStatisticChart.getChart_line().setVisibleXRangeMaximum(lineDataSet.getXMax());
            }
        } else if(type== TYPE_ITEM_CIRCLE){
            final HolderForStatisticCircle holderForStatisticCircle = (HolderForStatisticCircle) holder;
            StatisticCircleChart statisticCircleChart = (StatisticCircleChart) data.get(holderForStatisticCircle.getAdapterPosition());

            AnimatedPieViewConfig config = new AnimatedPieViewConfig();
            config.startAngle(-90)// Starting angle offset
                    .addData(new SimplePieInfo(statisticCircleChart.getHardTrain()
                            , holderForStatisticCircle.itemView.getResources().getColor(R.color.main_background_for)
                            , holderForStatisticCircle.itemView.getContext().getString(R.string.hard_circle_chart)))
                    .addData(new SimplePieInfo(statisticCircleChart.getCardioTrain()
                            , holderForStatisticCircle.itemView.getResources().getColor(R.color.level_midle)
                            , holderForStatisticCircle.itemView.getContext().getString(R.string.cardio_circle_chart)))
                    .addData(new SimplePieInfo(statisticCircleChart.getGroupTrain()
                            , holderForStatisticCircle.itemView.getResources().getColor(R.color.level_easy)
                            , holderForStatisticCircle.itemView.getContext().getString(R.string.group_circle_chart)))
                    .floatExpandAngle(15f)
                    .floatShadowRadius(18f)
                    .strokeMode(true)
                    .strokeWidth(45)
                    .drawText(true)
                    .canTouch(true)
                    .selectListener(new OnPieSelectListener<IPieInfo>() {
                        @Override
                        public void onSelectPie(@NonNull IPieInfo pieInfo, boolean isFloatUp) {
                            if(isFloatUp) {
                                Toast.makeText(holderForStatisticCircle.itemView.getContext(), String.valueOf(pieInfo.getValue()), Toast.LENGTH_SHORT).show();
                            }
                        }})
                    .textSize(16)
                    .guideLineWidth(4)// Text guide line stroke width
                    .guideLineMarginStart(8)// Guide point margin from chart
                    .duration(1000);// draw pie animation duration
            holderForStatisticCircle.getmAnimatedPieView().applyConfig(config);
            holderForStatisticCircle.getmAnimatedPieView().start();
            holderForStatisticCircle.getHard_dat().setText(String.valueOf(statisticCircleChart.getHardTrain()));
            holderForStatisticCircle.getCard_dat().setText(String.valueOf(statisticCircleChart.getCardioTrain()));
            holderForStatisticCircle.getGroup_dat().setText(String.valueOf(statisticCircleChart.getGroupTrain()));
        }
    }

    private void updateDesignChart(HolderForStatisticChart holderForStatisticChart, LineDataSet lineDataSet) {
        holderForStatisticChart.getChart_line().getDescription().setTextSize(10);
        lineDataSet.setValueTextSize(10f);
        lineDataSet.setFormSize(12f);
//        lineDataSet.setFillAlpha(110);
        lineDataSet.setLineWidth(2.0f);
        lineDataSet.setCircleRadius(4f);
        lineDataSet.setHighLightColor(Color.rgb(244, 117, 117));

        switch (holderForStatisticChart.getAdapterPosition()){
        case TONNAGE_CHART:
            lineDataSet.setCircleColor(Color.BLUE);
            lineDataSet.setColor(Color.BLUE);
            lineDataSet.setFillColor(Color.BLUE);
            break;
            case KPW_CHART:
            holderForStatisticChart.getChart_line().getDescription().setTextSize(10);
            lineDataSet.setCircleColor(Color.RED);
            lineDataSet.setColor(Color.RED);
            lineDataSet.setFillColor(Color.RED);
            break;
            case INTENCITY_CHART:
            lineDataSet.setCircleColor(Color.BLACK);
            lineDataSet.setColor(Color.BLACK);
            lineDataSet.setFillColor(Color.BLACK);
            break;
            case WEIGHT_CHART:
            lineDataSet.setCircleColor(Color.DKGRAY);
            lineDataSet.setColor(Color.DKGRAY);
            lineDataSet.setFillColor(Color.DKGRAY);
            break;
    }}

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
                if(data.get(position)!=null){
                int type = data.get(position).getItemType();
                if(type==TYPE_ITEM_OTHER_DATA){
                    return TYPE_ITEM_OTHER_DATA;
                } else {
                    return TYPE_ITEM_CIRCLE;
                }
}else return TYPE_ITEM_CIRCLE;}
}
