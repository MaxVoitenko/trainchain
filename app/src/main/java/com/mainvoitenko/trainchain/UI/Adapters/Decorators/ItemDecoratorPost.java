package com.mainvoitenko.trainchain.UI.Adapters.Decorators;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterPost;
import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderForAccountFriend;


public class ItemDecoratorPost  extends RecyclerView.ItemDecoration{

    private final int mSpace;
    private int countItems;

    public ItemDecoratorPost(int countItems) {
        this.mSpace = -55;
        this.countItems = countItems;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);

        RecyclerView.ViewHolder viewHolder = parent.getChildViewHolder(view);
        if(viewHolder instanceof RvAdapterPost.MoreItemss ||
                viewHolder instanceof HolderForAccountFriend){
           return;
        }
        if (position != 0) {
            outRect.bottom = mSpace;
        }
    }
}
