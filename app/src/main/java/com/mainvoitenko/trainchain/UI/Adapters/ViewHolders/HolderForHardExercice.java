package com.mainvoitenko.trainchain.UI.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;

import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class HolderForHardExercice extends RecyclerView.ViewHolder {

    @BindView(R.id.numberExercise)ImageView numberExercise;
    @BindView(R.id.nameExercise)AutoCompleteTextView nameExercise;
    @BindView(R.id.timerExerciseBtn)EditText timerExerciseBtn;
    @BindView(R.id.deleteEx) View deleteEx;

    @BindViews({R.id.count1, R.id.count2, R.id.count3, R.id.count4, R.id.count5, R.id.count6}) List<EditText> count;
    @BindViews({R.id.weight1, R.id.weight2, R.id.weight3, R.id.weight4, R.id.weight5, R.id.weight6}) List<EditText> weight;

    public HolderForHardExercice(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public ImageView getNumberExercise() {
        return numberExercise;
    }

    public List<EditText> getCount() {
        return count;
    }

    public List<EditText> getWeight() {
        return weight;
    }

    public void setCount(List<EditText> count) {
        this.count = count;
    }


    public AutoCompleteTextView getNameExercise() {
        return nameExercise;
    }

    public EditText getTimerExerciseBtn() {
        return timerExerciseBtn;
    }

    public View getDeleteEx() {
        return deleteEx;
    }
}