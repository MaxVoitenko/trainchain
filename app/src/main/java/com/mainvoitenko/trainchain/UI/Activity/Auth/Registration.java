package com.mainvoitenko.trainchain.UI.Activity.Auth;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.jakewharton.rxbinding3.view.RxView;
import com.mainvoitenko.trainchain.Providers.Net.CheckAchievement;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;


public class Registration extends AppCompatActivity{
    @BindView(R.id.emailEdit) EditText emailEdit;
    @BindView(R.id.nameEdit) EditText nameEdit;
    @BindView(R.id.passOneEdit) EditText passOneEdit;
    @BindView(R.id.passTwoEdit) EditText passTwoEdit;
    @BindView(R.id.registr) Button registr;

    private CompositeDisposable disposable;
    private FirebaseAuth mAuth;
    private DatabaseReference myRef;
    private FirebaseUser user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        myRef = SingleTonUser.getInstance().getMyRef();
        mAuth = SingleTonUser.getInstance().getmAuth();

    }

    @SuppressLint("CheckResult")
    @Override
    protected void onStart() {
        super.onStart();
        disposable = new CompositeDisposable();
        RxView.clicks(registr)
                .filter(unit -> !emailEdit.getText().toString().isEmpty() || nameEdit.getText().toString().isEmpty() ||
                        passOneEdit.getText().toString().isEmpty() || passTwoEdit.getText().toString().isEmpty())
                .filter(unit -> !passOneEdit.getText().toString().equals(passTwoEdit.getText().toString()))
                .subscribe(unit -> {createAccount(emailEdit.getText().toString(), passOneEdit.getText().toString());
                    Toast.makeText(Registration.this, R.string.wait_fr, Toast.LENGTH_SHORT).show();});
    }

    @Override
    protected void onStop() {
        disposable.dispose();
        super.onStop();
    }

//    @Optional @OnClick(R.id.registr)
//    public void registrClick(View v) {
//         if(emailEdit.getText().toString().isEmpty() || nameEdit.getText().toString().isEmpty() ||
//                 passOneEdit.getText().toString().isEmpty() || passTwoEdit.getText().toString().isEmpty()){
//             Toast.makeText(Registration.this, R.string.add_free_line, Toast.LENGTH_SHORT).show();
//         }else{
//             if(!passOneEdit.getText().toString().equals(passTwoEdit.getText().toString())){
//                 Toast.makeText(Registration.this, R.string.pass_not_equal, Toast.LENGTH_SHORT).show();
//             }else{
//                 Toast.makeText(Registration.this, R.string.wait_fr, Toast.LENGTH_SHORT).show();
//                 createAccount(emailEdit.getText().toString(), passOneEdit.getText().toString());
//             }}
//    }

//    @SuppressLint("CheckResult")
    private void createAccount(String email, String password) {    //Метод !регистрация!
//
//        RxFirebaseAuth
//                .createUserWithEmailAndPassword(mAuth,email,password)
//                .subscribeOn(Schedulers.io())
//                .subscribe(new Consumer<AuthResult>() {
//                    @Override
//                    public void accept(AuthResult authResult) throws Exception {
//                        user = authResult.getUser();
//                    }
//                });

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    user = SingleTonUser.getInstance().getUser();
                    myRef.child(user.getUid()).runTransaction(new Transaction.Handler() {
                        @Override
                        public Transaction.Result doTransaction(MutableData mutableData) {//todo here
                            mutableData.child(SettingsConstants.NAME).setValue(nameEdit.getText().toString());
                            mutableData.child(SettingsConstants.PERSONAL_ID).setValue(user.getUid());

                            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    CheckAchievement checkAchievement = new CheckAchievement(dataSnapshot,user.getUid(), Registration.this);
                                    checkAchievement.firstTen();
                                    checkAchievement.firstFifty();
                                    checkAchievement.firstOneHundred();
                                }@Override public void onCancelled(DatabaseError databaseError) {}});

                            return Transaction.success(mutableData);}
                        @Override public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                                Toast.makeText(Registration.this, R.string.registr_is_successful, Toast.LENGTH_SHORT).show();
                                finish();
                        }});
                } else { Toast.makeText(Registration.this, R.string.registr_not_successful, Toast.LENGTH_SHORT).show(); }
            }
        });
    }
}
