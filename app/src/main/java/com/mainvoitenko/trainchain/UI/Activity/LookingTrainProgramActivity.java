package com.mainvoitenko.trainchain.UI.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mainvoitenko.trainchain.Model.TrainPrograms;
import com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders.TrainProgramsTxtProvider;
import com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders.OneTrainProgramProvider;
import com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders.SendLikeProvider;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class LookingTrainProgramActivity extends AppCompatActivity implements OneTrainProgramProvider.Callback
                                                                , SendLikeProvider.Callback
                                                                , TrainProgramsTxtProvider.Callback {

    public static  final int REQUEST_UPDATE_TRAININGPROGRAM= 1558;

    @BindView(R.id.trainProgram) TextView trainProgram;
    @BindView(R.id.titleProgram) TextView titleProgram;
    @BindView(R.id.imageViewBack) ImageView imageViewBack;
    @BindView(R.id.likeThisProgram) ImageButton likeThisProgram;
    @BindView(R.id.sendError) ImageButton sendError;
    @BindView(R.id.repostTrain) ImageButton repostTrain;

    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();

    private String programId = "";
    private TrainPrograms traininigPrograms;
    private int position;
    private OneTrainProgramProvider trainProvider;
    private TrainProgramsTxtProvider trainProgramsTxtProvider;

    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_program);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        programId = intent.getStringExtra(SettingsConstants.PROGRAM_ID_INTENT);
        int typePic = intent.getIntExtra(SettingsConstants.PROGRAM_TYPE_PIC_INTENT,0);
        position = intent.getIntExtra(SettingsConstants.PROGRAM_POSITION,-1);
        String title = intent.getStringExtra(SettingsConstants.PROGRAM_TITLE_INTENT);
        if(title!= null){
            titleProgram.setText(title);
        }
        Picasso.with(this).load(typePic).into(imageViewBack);

        trainProvider = new OneTrainProgramProvider(programId, user.getUid());
        trainProvider.registerCallBack(this);
        myRef.addListenerForSingleValueEvent(trainProvider);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.wait_fr));
        progressDialog.show();
        trainProgramsTxtProvider = new TrainProgramsTxtProvider(this);
        trainProgramsTxtProvider.registerCallBack(this);
        trainProgramsTxtProvider.loadTxtProgram(programId);
    }

    @Optional  @OnClick(R.id.likeThisProgram)
    public void likeThisProgramClick(View v) {
        SendLikeProvider sendLikeProvider = new SendLikeProvider(myRef,user.getUid(),programId);
        sendLikeProvider.registerCallBack(this);
        myRef.addListenerForSingleValueEvent(sendLikeProvider);
    }

    @Optional @OnClick(R.id.sendError)
    public void sendErrorClick(View v) {
        Toast.makeText(this, R.string.wait_fr, Toast.LENGTH_SHORT).show();
        Intent emailIntent = new Intent(android.content.Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:?subject=" + "Ошибка в программе:"+ programId
                + "&body=" + "" + "&to=" + "mainvoitenko@gmail.com");
        emailIntent.setData(data);
        startActivity(Intent.createChooser(emailIntent, "send mail"));
    }

    @Optional @OnClick(R.id.repostTrain)
    public void repostTrainClick(View v) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,  "#TrainChain for Android\n" +  trainProgram.getText().toString()
                + "\n\n(https://play.google.com/store/apps/details?id=com.mainvoitenko.trainchain)");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Optional @OnClick(R.id.clickBack)
    public void clickBackClick(View v) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        int like = SettingsConstants.LIKE_NOW_YES;
        if (traininigPrograms.getMyLike()!= SettingsConstants.LIKE_NOW_YES){
            like=SettingsConstants.LIKE_NOW_NO; }
        Intent intent = new Intent();
        intent.putExtra(SettingsConstants.PROGRAM_POSITION, position);
        intent.putExtra(SettingsConstants.ADD_LIKE,like);
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }

    @Override
    public void callingBackOneTrainProgram(TrainPrograms trainPrograms) {
        traininigPrograms = trainPrograms;
        if (trainPrograms.getMyLike() == SettingsConstants.LIKE_NOW_YES) {
            likeThisProgram.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_to_favorite));
        } else {
            likeThisProgram.setImageDrawable(getResources().getDrawable(R.drawable.ic_remove_favorite_white));
        }
    }

    @Override
    public void callingBackTrainProgramsLike(int isLike) {
            if(isLike == SettingsConstants.LIKE_NOW_YES){
                likeThisProgram.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_to_favorite));
                traininigPrograms.setMyLike(SettingsConstants.LIKE_NOW_YES);
            }else{
                likeThisProgram.setImageDrawable(getResources().getDrawable(R.drawable.ic_remove_favorite_white));
                traininigPrograms.setMyLike(SettingsConstants.LIKE_NOW_NO);
            }
    }

    @Override
    public void callingBackTrainProgramsTxt(StringBuilder stringBuilder) {
        progressDialog.dismiss();
        if(stringBuilder!=null){trainProgram.setText(stringBuilder);}
    }

    @Override
    protected void onDestroy() {
        myRef.removeEventListener(trainProvider);
        super.onDestroy();
    }
}
