package com.mainvoitenko.trainchain.UI.Adapters.ViewHolders;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mainvoitenko.trainchain.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.annotations.Nullable;

public class HolderForAccountFriend extends RecyclerView.ViewHolder {

    @Nullable @BindView(R.id.cityTextView) TextView cityTextView;
    @Nullable @BindView(R.id.heightTextView) TextView heightTextView;
    @Nullable @BindView(R.id.countTrainFriend) TextView countTrainFriend;
    @Nullable @BindView(R.id.urlTextView) TextView urlTextView;
    @Nullable @BindView(R.id.textQualAcc) TextView textQualAcc;
    @Nullable @BindView(R.id.friendsCount) TextView friendsCount;
    @Nullable @BindView(R.id.followersCount) TextView followersCount;
    @Nullable @BindView(R.id.buttonFriend) TextView buttonFriend;
    @Nullable @BindView(R.id.buttonFollows) TextView buttonFollows;
    @Nullable @BindView(R.id.progress_pic) ProgressBar progress_pic;

    public HolderForAccountFriend(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public TextView getCityTextView() {
        return cityTextView;
    }

    public TextView getUrlTextView() {
        return urlTextView;
    }

    public TextView getHeightTextView() {
        return heightTextView;
    }

    public TextView getCountTrainFriend() {
        return countTrainFriend;
    }

    public TextView getTextQualAcc() {
        return textQualAcc;
    }

    public ProgressBar getProgress_pic() {
        return progress_pic;
    }

    public TextView getFriendsCount() {
        return friendsCount;
    }

    public TextView getFollowersCount() {
        return followersCount;
    }

    public TextView getButtonFriend() {
        return buttonFriend;
    }

    public TextView getButtonFollows() {
        return buttonFollows;
    }
}