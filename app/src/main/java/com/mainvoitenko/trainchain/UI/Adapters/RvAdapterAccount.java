package com.mainvoitenko.trainchain.UI.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.mainvoitenko.trainchain.Account.Activity.SearchAccountActivity;
import com.mainvoitenko.trainchain.Interface.Clickers.OnLikeProgramClick;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderForAccount;
import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderForTrainPrograms;
import com.mainvoitenko.trainchain.Account.Entity.AccountMy;
import com.mainvoitenko.trainchain.Model.TrainPrograms;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Interface.Clickers.OnTrainingProgramClickListener;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.Account.Activity.AccountOtherUserActivity;
import com.mainvoitenko.trainchain.Account.Activity.AccountDataFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RvAdapterAccount extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();


    private ArrayList<ItemGrouper> items;
    private final int TYPE_ITEM_ACC = SettingsConstants.GROUPER_MY_ACCOUNT;
    private final int TYPE_ITEM_PROGRAM = SettingsConstants.GROUPER_TRAINING;
    private final int TYPE_ITEM_PROGRAM_ZERO = SettingsConstants.GROUPER_MOREE;

    private OnTrainingProgramClickListener onTrainingProgramClickListener;
    private OnIntentToPrograms onIntentToPrograms;
    private OnLikeProgramClick onLikeProgramClick;


    public void setOnZeroProgramClickListener(OnIntentToPrograms onIntentToPrograms){
        this.onIntentToPrograms= onIntentToPrograms;
    }
    public void setOnLikeProgramClick(OnLikeProgramClick onLikeProgramClick) {
        this.onLikeProgramClick = onLikeProgramClick; }


    public RvAdapterAccount(ArrayList<ItemGrouper> items) {
        this.items = items;
    }
    public void setOnTrainingProgramClickListener(OnTrainingProgramClickListener onTrainingProgramClickListener){
        this.onTrainingProgramClickListener= onTrainingProgramClickListener;}

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case TYPE_ITEM_PROGRAM:
                holder = new HolderForTrainPrograms(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_training_programs, parent, false));
                break;
            case TYPE_ITEM_ACC:
                holder = new HolderForAccount(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_account, parent, false));
                break;
            default: //аккаунт
                holder = new HolderUnivesal(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_moreee, parent, false));
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(holder.getAdapterPosition());
        if (type == TYPE_ITEM_PROGRAM) {
            ProgramsHolderBind(holder);
        } else if(type == TYPE_ITEM_ACC){
            AccountHolderBind(holder);
        }else if(type ==TYPE_ITEM_PROGRAM_ZERO){
            HolderUnivesal holderUnivesal = (HolderUnivesal) holder;
        }
    }

   @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(items.get(position) != null){
        int type = items.get(position).getItemType();
        if (type == TYPE_ITEM_ACC) return TYPE_ITEM_ACC; else if (type == TYPE_ITEM_PROGRAM) return TYPE_ITEM_PROGRAM;
        else  return TYPE_ITEM_PROGRAM_ZERO;
        }else {return TYPE_ITEM_PROGRAM_ZERO;}
    }



    public class HolderUnivesal extends RecyclerView.ViewHolder implements View.OnClickListener{
        public HolderUnivesal(View itemView){
            super(itemView);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            if (onIntentToPrograms != null) {
                onIntentToPrograms.onIntentToPrograms();
            }
        }
    }

    public interface OnIntentToPrograms {
        void onIntentToPrograms();
    }


    private void ProgramsHolderBind(final RecyclerView.ViewHolder holder){
        final HolderForTrainPrograms holderProg = (HolderForTrainPrograms) holder;
        final TrainPrograms accTrain = (TrainPrograms) items.get(holder.getAdapterPosition());

        holderProg.getTitleProgram().setText(accTrain.getTitle());
        holderProg.getDurationProgram().setText(accTrain.getDuration());
        holderProg.getGoalProgram().setText(accTrain.getGoal());
        holderProg.getLevelProgram().setText(accTrain.getLevel());
        holderProg.getAuthorProgram().setText(accTrain.getAuthor());
        holderProg.getLikeCount().setText(accTrain.getLike());
        holderProg.getDayInWeekProgram().setText(accTrain.getDayInWeek());
        holderProg.getAddNameAutoProgram().setImageDrawable(null);
        if(!accTrain.getAutoAdder().equals("-")){
            holderProg.getAddNameAutoProgram().setImageDrawable(holder.itemView.getResources().getDrawable(R.drawable.ic_writing));
        }

        linkPerson(holderProg, accTrain);

        Picasso.with(holder.itemView.getContext()).load(accTrain.getResourceBackground()).into(holderProg.getImageViewBack());


        if (accTrain.getMyLike() == SettingsConstants.LIKE_NOW_YES) {
            holderProg.getLikeProgram().setImageDrawable(holderProg.itemView.getResources().getDrawable(R.drawable.ic_add_to_favorite));
        } else {
            holderProg.getLikeProgram().setImageDrawable(holderProg.itemView.getResources().getDrawable(R.drawable.ic_remove_favorite));
        }
        levelColor(holderProg, accTrain);

        holderProg.getLikeProgram().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onLikeProgramClick!= null) {
                    onLikeProgramClick.setOnLikeProgramClick(accTrain.getProgramId(), holder.getAdapterPosition());}
            }
        });

        View.OnClickListener showProgram = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onTrainingProgramClickListener != null) {
                    onTrainingProgramClickListener.setOnTrainingProgramClickListener(
                            accTrain.getProgramId()
                            ,holderProg.getImageViewBack()
                            ,accTrain.getResourceBackground()
                            ,accTrain.getTitle()
                            ,holderProg.getAdapterPosition());
                }
            }
        };
        holderProg.getShowFullProgram().setOnClickListener(showProgram);
        holderProg.getTitleProgram().setOnClickListener(showProgram);
        holderProg.getImageViewBack().setOnClickListener(showProgram);
    }

    private void AccountHolderBind(RecyclerView.ViewHolder holder){
        final HolderForAccount holderForAccount = (HolderForAccount) holder;
        AccountMy accountMy = (AccountMy) items.get(holderForAccount.getAdapterPosition());
        holderForAccount.getCityTextView().setText(accountMy.getCityGym());
        holderForAccount.getHeightTextView().setText(accountMy.getHeightWeight());
        holderForAccount.getFollowersCount().setText(accountMy.getCountMyFollowers());
        holderForAccount.getFriendsCount().setText(accountMy.getCountMyFriends());

        updateQualificationAccount(holderForAccount, accountMy);


        View.OnClickListener AccountListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                switch (v.getId()) {
                    case R.id.friendsCount:
                        intent= new Intent(holderForAccount.itemView.getContext(), SearchAccountActivity.class);
                        intent.putExtra(SettingsConstants.SEARCH_DATA_INTENT, SettingsConstants.MY_FRIEND);
                        intent.putExtra(SettingsConstants.SEARCH_ID_INTENT_STRING, user.getUid());
                        ((Activity)holderForAccount.itemView.getContext()).startActivityForResult(intent, AccountDataFragment.REQUEST_NEW_DATA);
                        break;
                    case R.id.buttonFriend:
                        intent= new Intent(holderForAccount.itemView.getContext(), SearchAccountActivity.class);
                        intent.putExtra(SettingsConstants.SEARCH_DATA_INTENT, SettingsConstants.MY_FRIEND);
                        intent.putExtra(SettingsConstants.SEARCH_ID_INTENT_STRING, user.getUid());
                        ((Activity)holderForAccount.itemView.getContext()).startActivityForResult(intent, AccountDataFragment.REQUEST_NEW_DATA);
                        break;
                    case R.id.buttonFollows:
                        intent= new Intent(holderForAccount.itemView.getContext(), SearchAccountActivity.class);
                        intent.putExtra(SettingsConstants.SEARCH_DATA_INTENT, SettingsConstants.MY_FOLLOWERS);
                        intent.putExtra(SettingsConstants.SEARCH_ID_INTENT_STRING, user.getUid());
                        ((Activity)holderForAccount.itemView.getContext()).startActivityForResult(intent , AccountDataFragment.REQUEST_NEW_DATA);
                        break;
                    case R.id.followersCount:
                        intent= new Intent(holderForAccount.itemView.getContext(), SearchAccountActivity.class);
                        intent.putExtra(SettingsConstants.SEARCH_DATA_INTENT, SettingsConstants.MY_FOLLOWERS);
                        intent.putExtra(SettingsConstants.SEARCH_ID_INTENT_STRING, user.getUid());
                        ((Activity)holderForAccount.itemView.getContext()).startActivityForResult(intent , AccountDataFragment.REQUEST_NEW_DATA);
                        break;

                }
            }
        };

        holderForAccount.getFriendsCount().setOnClickListener(AccountListener);
        holderForAccount.getButtonFriend().setOnClickListener(AccountListener);

        holderForAccount.getFollowersCount().setOnClickListener(AccountListener);
        holderForAccount.getButtonFollows().setOnClickListener(AccountListener);

    }

    private void updateQualificationAccount(final HolderForAccount holderForAccount, AccountMy accountMy){
        if(accountMy.getQualificationAccount().equals("2")){
            holderForAccount.getUrlTextView().setVisibility(View.VISIBLE);
            if(!accountMy.getMyUrlAdress().equals("")){
            holderForAccount.getUrlTextView().setText(accountMy.getMyUrlAdress());
            holderForAccount.getUrlTextView().setPaintFlags(holderForAccount.getUrlTextView().getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holderForAccount.getUrlTextView().setTextColor(holderForAccount.itemView.getResources().getColor(R.color.link_program));
            holderForAccount.getUrlTextView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(holderForAccount.itemView.getContext(),  R.string.wait_fr, Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(holderForAccount.getUrlTextView().getText().toString()));
                    holderForAccount.itemView.getContext().startActivity(i);
                }
            });
        }}
    }
    private void linkPerson(final HolderForTrainPrograms holderProg, final TrainPrograms accTrain) {
        if(accTrain.getPersonId().equals("-1")) {
            holderProg.getAuthorProgram().setTextColor(holderProg.itemView.getResources().getColor(R.color.text_light_gray));

        }else{
            holderProg.getAuthorProgram().setPaintFlags(holderProg.getAuthorProgram().getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holderProg.getAuthorProgram().setTextColor(holderProg.itemView.getResources().getColor(R.color.link_program));
            holderProg.getAuthorProgram().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(holderProg.itemView.getContext(), AccountOtherUserActivity.class);
                    intent.putExtra(SettingsConstants.PERSON_ID_INTENT, accTrain.getPersonId());
                    holderProg.itemView.getContext().startActivity(intent);
                }
            });
        }
    }
    private void levelColor(HolderForTrainPrograms holder, TrainPrograms accTrain){
        if(accTrain.getLevel().equals(holder.itemView.getResources().getString(R.string.level_easy_txt))) {
            holder.getLevelProgram().setTextColor(holder.itemView.getResources().getColor(R.color.level_easy));
        }else if(accTrain.getLevel().equals(holder.itemView.getResources().getString(R.string.level_midle_txt))){
            holder.getLevelProgram().setTextColor(holder.itemView.getResources().getColor(R.color.level_midle));
        }else if(accTrain.getLevel().equals(holder.itemView.getResources().getString(R.string.level_hard_txt))){
            holder.getLevelProgram().setTextColor(holder.itemView.getResources().getColor(R.color.icon_red));
        }else{
            holder.getLevelProgram().setTextColor(holder.itemView.getResources().getColor(R.color.text_light_gray));
        }
    }
}
