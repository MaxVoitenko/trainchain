package com.mainvoitenko.trainchain.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders.AllTrainProgramsProvider;
import com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders.SendLikeProvider;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterTrainingPrograms;
import com.mainvoitenko.trainchain.Model.TrainPrograms;
import com.mainvoitenko.trainchain.Interface.Clickers.OnLikeProgramClick;
import com.mainvoitenko.trainchain.Interface.Clickers.OnTrainingProgramClickListener;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.UI.Activity.LookingTrainProgramActivity;

import java.util.ArrayList;

public class TrainProgramsFragment  extends Fragment implements OnTrainingProgramClickListener
                                                    , OnLikeProgramClick
                                                    , AllTrainProgramsProvider.Callback
                                                    , SendLikeProvider.Callback {

    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();

    private RecyclerView rvTrainPrograms;
    private ProgressBar progress_pic;
    private RvAdapterTrainingPrograms rvAdapterTrainingPrograms;
    private ArrayList<TrainPrograms> trainPrograms;

    private SendLikeProvider sendLikeProvider;
    private AllTrainProgramsProvider allTrainProgramsProvider;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_train_programs, container,false);
        rvTrainPrograms = mView.findViewById(R.id.rvTrainProg);
        progress_pic = mView.findViewById(R.id.progress_pic);

        progress_pic.setVisibility(View.VISIBLE);
        trainPrograms = new ArrayList<>();
        myRef.keepSynced(true);

        rvAdapterTrainingPrograms = new RvAdapterTrainingPrograms(trainPrograms);
        rvAdapterTrainingPrograms.setOnTrainingProgramClickListener(this);
        rvAdapterTrainingPrograms.setOnLikeProgramClick(this);
        rvTrainPrograms.setAdapter(rvAdapterTrainingPrograms);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        rvTrainPrograms.setLayoutManager(mLayoutManager);
        rvTrainPrograms.setHasFixedSize(true);
        addData();

        return mView;
    }

    private void addData(){
        allTrainProgramsProvider = new AllTrainProgramsProvider(user.getUid());
        allTrainProgramsProvider.registerCallBack(this);
        myRef.addListenerForSingleValueEvent(allTrainProgramsProvider);
    }

    @Override
    public void setOnTrainingProgramClickListener(String idTrain, View v1, int type, String title, int position) {
        if(idTrain!= null && !idTrain.isEmpty()) {
            Intent intent = new Intent(getContext(), LookingTrainProgramActivity.class);
            intent.putExtra(SettingsConstants.PROGRAM_ID_INTENT, idTrain);
            intent.putExtra(SettingsConstants.PROGRAM_TYPE_PIC_INTENT, type);
            intent.putExtra(SettingsConstants.PROGRAM_TITLE_INTENT, title);
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    getActivity(),
                    new Pair<View, String>(v1.findViewById(R.id.imageViewBack),
                            getString(R.string.transition_name_circle)));
            ActivityCompat.startActivity(getActivity(), intent, options.toBundle());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
             addData();
    }

    @Override
    public void callingBackAllTrainPrograms(ArrayList<TrainPrograms> allTrainPrograms) {
        if(allTrainPrograms!=null) {
            trainPrograms.clear();
            trainPrograms.addAll(allTrainPrograms);
            rvAdapterTrainingPrograms.notifyDataSetChanged();
        }
        progress_pic.setVisibility(View.GONE);
    }

    @Override
    public void callingBackTrainProgramsLike(int isLike) {
        addData();
    }

    @Override
    public void setOnLikeProgramClick(String idProgram, int position) {
        sendLikeProvider = new SendLikeProvider(myRef,user.getUid(),idProgram);
        sendLikeProvider.registerCallBack(this);
        myRef.addListenerForSingleValueEvent(sendLikeProvider);
    }

    @Override
    public void onDestroy() {
//        myRef.removeEventListener(sendLikeProvider);
        myRef.removeEventListener(allTrainProgramsProvider);
        super.onDestroy();
    }
}
