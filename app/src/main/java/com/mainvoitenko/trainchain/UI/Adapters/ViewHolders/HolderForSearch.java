package com.mainvoitenko.trainchain.UI.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.mainvoitenko.trainchain.R;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class HolderForSearch extends RecyclerView.ViewHolder {
    @BindView(R.id.imageProfile) RoundedImageView imageProfile;
    @BindView(R.id.friendName) TextView friendName;
    @BindView(R.id.friendCity) TextView friendCity;
    @BindView(R.id.qualificationTypeAccount) TextView qualificationTypeAccount;

    public HolderForSearch(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public TextView getFriendName() {
        return friendName;
    }

    public TextView getFriendCity() {
        return friendCity;
    }

    public RoundedImageView getImageProfile() {
        return imageProfile;
    }

    public TextView getQualificationTypeAccount() {
        return qualificationTypeAccount;
    }
}