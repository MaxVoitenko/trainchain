package com.mainvoitenko.trainchain.UI.CustomView;


import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.TypedValue;

/**
 * Created by Администратор on 02.04.2018.
 */

public class TextViewAutoText extends AppCompatTextView {
    public TextViewAutoText(Context context) {
        super(context, null);
    }

    public TextViewAutoText(Context context, AttributeSet attrs) {
        super(context, attrs, android.R.attr.textViewStyle);
    }

    public TextViewAutoText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) { //каких размеров ВЬЮ

        int unspecMesureSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED); // рахмер ЛЮБОЙ по длинне

        int maxWidth = MeasureSpec.getSize(widthMeasureSpec);
        float textSize = getTextSize();

        if(getMeasuredWidth() > maxWidth){
            float temp = maxWidth/((float)getMeasuredWidth());
            textSize = getTextSize()*temp; // в соотношениях
        }

        do { //подгоняю по макро
            setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize); //подменили шрифт размер  (текст в пикселях задавать, задаю размер)
            super.onMeasure(unspecMesureSpec, heightMeasureSpec);

            textSize -=10;

        }while (getMeasuredWidth() > maxWidth);

        // super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) { // ГДЕ РАСПОЛОЖЕНА
        super.onLayout(changed, left, top, right, bottom);
    }
}

