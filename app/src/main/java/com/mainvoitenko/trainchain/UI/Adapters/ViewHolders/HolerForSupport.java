package com.mainvoitenko.trainchain.UI.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mainvoitenko.trainchain.R;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class HolerForSupport extends RecyclerView.ViewHolder{
    @BindView(R.id.askText) TextView askText;
    @BindView(R.id.answerText) TextView answerText;

    public HolerForSupport(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public TextView getAskText() {
        return askText;
    }

    public TextView getAnswerText() {
        return answerText;
    }
}